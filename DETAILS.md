# Laravel 7 Notification Using Toastr
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel7-notification-using-toastr.git`
2. Go inside the folder: `cd laravel7-notification-using-toastr`
3. Run `php artisan serve`
4. Open your favorite browser: http://localhost:8000/notif

### Screen shot

Plain Page

![Plain Page](img/plain.png "Plain Page")

Error Page

![Error Page](img/error.png "Error Page")

Info Page

![Info Page](img/info.png "Info Page")

Success Page

![Success Page](img/success.png "Success Page")

Warning Page

![Warning Page](img/warning.png "Warning Page")
